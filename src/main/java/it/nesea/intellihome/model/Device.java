package it.nesea.intellihome.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Setter @Getter @ToString
@Entity
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_id_seq_gen")
    @SequenceGenerator(name = "device_id_seq_gen", sequenceName = "device_id_seq")
    Integer id;
    String name;
    Double power;

    @OneToMany(mappedBy = "room", fetch = FetchType.LAZY)
    private Set<DevicePerRoom> rooms = new HashSet<>();
}
