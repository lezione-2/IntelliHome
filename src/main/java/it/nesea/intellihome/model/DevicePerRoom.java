package it.nesea.intellihome.model;

import lombok.*;

import javax.persistence.*;

@Getter @Setter
@ToString
@Entity
@Table(name = "device_per_room")
public class DevicePerRoom {
    @EmbeddedId
    private DevicePerRoomId devicePerRoomId = new DevicePerRoomId();
    private Integer quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idDevice")
    @JoinColumn(name = "id_device")
    private Device device;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("idRoom")
    @JoinColumn(name = "id_room")
    private Room room;

}
