package it.nesea.intellihome.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter @Setter
@Embeddable
public class DevicePerRoomId implements Serializable {

    @Column(name = "id_device")
    private Integer idDevice;
    @Column(name = "id_room")
    private Integer idRoom;
}
