package it.nesea.intellihome.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter @Setter
@ToString(exclude = "devices") //Abbiamo escluso questo campo per non creare cicli nelle bidirezionalità della relazione
@Entity
@Table(name = "Room")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "room_id_seq_gen")
    @SequenceGenerator(name = "room_id_seq_gen", sequenceName = "room_id_seq")
    Integer id;
    String name;

    @OneToMany(mappedBy = "device", fetch = FetchType.LAZY)
    private Set<DevicePerRoom> devices = new HashSet<>();
}
