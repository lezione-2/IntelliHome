package it.nesea.intellihome;

import it.nesea.intellihome.devices.DeviceManager;
import it.nesea.intellihome.model.Device;
import it.nesea.intellihome.model.DevicePerRoom;
import it.nesea.intellihome.model.Room;
import it.nesea.intellihome.repositories.DevicePerRoomRepository;
import it.nesea.intellihome.repositories.DeviceRepository;
import it.nesea.intellihome.repositories.RoomRepository;
import it.nesea.intellihome.services.RoomService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

@SpringBootApplication
@PropertySource("classpath:devices.properties")
public class IntelliHomeApplication {
        public static void main(String[] args) {
            ConfigurableApplicationContext run = SpringApplication.run(IntelliHomeApplication.class, args);
            for (String beanName : run.getBeanDefinitionNames()){
                System.out.println("beanName = " + beanName);
            }

            readProperties(run);
            saveRoom(run);

            RoomService roomService = (RoomService) run.getBean("roomService");
            List<DeviceManager> roomDevices = roomService.getRoomDevices(1);
            double v = roomService.calculateCurrentElecticPowerCost(roomDevices);
            System.out.println("Costo tutto spento = " + v);

            roomService.powerOnAllDevices(roomDevices);
            v = roomService.calculateCurrentElecticPowerCost(roomDevices);
            System.out.println("Costo tutto acceso = " + v);
            List<Room> roomsByDevice = roomService.getRoomsByDevice(1);
            System.out.println("roomsByDevice = " + roomsByDevice);
        }

    private static void readProperties(ConfigurableApplicationContext run) {
        Resource resource = run.getResource("classpath:devices.properties");
        try{
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void saveRoom(ConfigurableApplicationContext run){
        RoomRepository roomRepository = (RoomRepository)run.getBean("roomRepository");
        DeviceRepository deviceRepository = (DeviceRepository)run.getBean("deviceRepository");
        DevicePerRoomRepository devicePerRoomRepository = (DevicePerRoomRepository)run.getBean("devicePerRoomRepository");

        Room room = new Room();
        room.setName("Living Room");
        room = roomRepository.saveAndFlush(room);
        Device device = new Device();
        device.setName("abatjour");
        device.setPower(25d);
        device = deviceRepository.saveAndFlush(device);

        DevicePerRoom devicePerRoom = new DevicePerRoom();
        devicePerRoom.setDevice(device);
        devicePerRoom.setQuantity(2);
        devicePerRoom.setRoom(room);
        devicePerRoomRepository.saveAndFlush(devicePerRoom);

    }
}
