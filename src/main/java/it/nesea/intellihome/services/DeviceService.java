package it.nesea.intellihome.services;

import it.nesea.intellihome.model.Device;
import it.nesea.intellihome.repositories.DevicePerRoomRepository;
import it.nesea.intellihome.repositories.DeviceRepository;
import it.nesea.intellihome.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Classe di servizio per i devices.
 */

@Service
public class DeviceService {
    @Value("${electicalcompany.kwperh.euro}")
    private Double kwperhEuro;

    private DeviceRepository deviceRepository;
    private DevicePerRoomRepository devicePerRoomRepository;
    private ApplicationContext applicationContext;

    public DeviceService(DeviceRepository deviceRepository, DevicePerRoomRepository devicePerRoomRepository, ApplicationContext applicationContext) {
        this.deviceRepository = deviceRepository;
        this.devicePerRoomRepository = devicePerRoomRepository;
        this.applicationContext = applicationContext;
    }

    public void addDevice(Device device){
        deviceRepository.save(device);
    }


}
