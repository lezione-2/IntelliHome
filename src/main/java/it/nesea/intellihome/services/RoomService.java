package it.nesea.intellihome.services;

import it.nesea.intellihome.devices.DeviceManager;
import it.nesea.intellihome.model.Device;
import it.nesea.intellihome.model.DevicePerRoom;
import it.nesea.intellihome.model.DevicePerRoomId;
import it.nesea.intellihome.model.Room;
import it.nesea.intellihome.repositories.DevicePerRoomRepository;
import it.nesea.intellihome.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Con questo servizio è possibile configurare varie stanze della propria casa.
 * Il servizio tiene in considerazione elementi di tipo device che possono essere aggiunti
 * all'interno dell'area.
 * Su tutti questi devie, ereditati come componenti prototype, sarà possibile agire
 * per compiere varie operazioni.
 * IMPORTANTE: Lo scope della classe è Singleton. In buona sostanza non abbiamo alcun interesse a creare più
 * Istanze, ma non per il motivo legato alla lezione 1, ma poiché adesso le stanze sono ben definite nel DB.
 * Attraverso questo service, noi ci limitiamo a conslutarle ed operare logiche su di esso.
 */
@Service
public class RoomService {

    @Value("${electicalcompany.kwperh.euro}")
    private Double kwperhEuro;

    private RoomRepository roomRepository;
    private DevicePerRoomRepository devicePerRoomRepository;
    private ApplicationContext applicationContext;

    /**
     * Constructor Autowiring: Non abbiamo bisogno di usare l'annotation
     * @Autoquied. Questa è implicita.
     * @param roomRepository
     * @param devicePerRoomRepository
     * @param applicationContext
     */
    public RoomService(RoomRepository roomRepository, DevicePerRoomRepository devicePerRoomRepository, ApplicationContext applicationContext) {
        this.roomRepository = roomRepository;
        this.devicePerRoomRepository = devicePerRoomRepository;
        this.applicationContext = applicationContext;
    }

    /**
     * Prima cerca se la stanza esiste, poi aggiunge il dispositivo.
     * @param device
     * @param roomId
     */
    public void assignDevice(Device device, Integer roomId){
        roomRepository.findById(roomId).ifPresent(room -> {
            DevicePerRoom devicePerRoom = new DevicePerRoom();
            devicePerRoom.setDevice(device);
            devicePerRoom.setQuantity(2);
            devicePerRoom.setRoom(room);
            devicePerRoomRepository.save(devicePerRoom);
        });
    }

    /**
     * In questa cancellazione, andiamo a chiedere la cancellazione per ID della relazione N::N
     * @param deviceId
     * @param roomId
     */
    public void removeDevice(Integer deviceId, Integer roomId){
        DevicePerRoomId devicePerRoomId = new DevicePerRoomId();
        devicePerRoomId.setIdDevice(deviceId);
        devicePerRoomId.setIdRoom(roomId);
        devicePerRoomRepository.deleteById(devicePerRoomId);
    }



    /**
     * Ritorna la lista dei devices manager di una particolare stanza
     * L'operatività concreta viene data dai Managr che ricordiamo essere
     * dei Prototype.
     * Questo significa che per mantenere coscienza degli stati, bisogna
     * mantenere in memory la collection risultante.
     * @param id
     * @return
     */
    public List<DeviceManager> getRoomDevices(Integer id){
        Optional<Room> byId = roomRepository.findById(id);
        List<DeviceManager> deviceManagers = new LinkedList<>();
        byId.ifPresent(room ->
                room.getDevices().forEach(devicePerRoom -> {
                    for(int i = 0; i < devicePerRoom.getQuantity(); i++) {
                        DeviceManager deviceManager = (DeviceManager)applicationContext.getBean(devicePerRoom.getDevice().getName());
                        deviceManager.setDevice(devicePerRoom.getDevice());
                        deviceManagers.add(deviceManager);
                    }
                }));
        return deviceManagers;
    }

    /**
     * Calcola la potenza in watt di tutti i dispositvi connessi nella stanza
     * @param deviceManagers
     * @return
     */
    public double calculateElectricPower(List<DeviceManager> deviceManagers) {
        return deviceManagers.stream().mapToDouble(value -> value.getDevice().getPower()).sum();
    }

    /**
     * Restituisce la spesa corrente dei dispositivi accesi all'interno di una stanza
     * @param deviceManagers
     * @return
     */
    public double calculateCurrentElecticPowerCost(List<DeviceManager> deviceManagers){
        AtomicReference<Double> powerPerRoom = new AtomicReference<>(0d);
        deviceManagers.forEach(deviceManager -> {
            if (deviceManager.isOn()){
                powerPerRoom.updateAndGet(v ->  (v + deviceManager.electricalCost(kwperhEuro)));
            }
        });
        return (powerPerRoom.get());
    }


    /**
     * Accende tutti i devices
     * @param deviceManagers
     */
    public void powerOnAllDevices(List<DeviceManager> deviceManagers){
        deviceManagers.forEach(DeviceManager::powerOn);
    }

    /**
     * Esempio di interrogazione del DB effettuando una JOIN in una N::N
     * @param deviceId
     * @return
     */
    public List<Room> getRoomsByDevice(Integer deviceId){
        return roomRepository.findByDevices_Device_Id(deviceId);
    }

}
