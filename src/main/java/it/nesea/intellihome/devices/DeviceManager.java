package it.nesea.intellihome.devices;

import it.nesea.intellihome.model.Device;

public interface DeviceManager {
    /**
     * Calcolo del costo KW/h
     * @param price
     * @return
     */
    double electricalCost(double price);
    Device getDevice();
    void setDevice(Device d);
    void powerOn();
    void powerOff();
    boolean isOn();
}
