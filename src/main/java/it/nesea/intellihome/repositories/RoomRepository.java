package it.nesea.intellihome.repositories;

import it.nesea.intellihome.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {
    List<Room> findByDevices_Device_Id(Integer id);
}
