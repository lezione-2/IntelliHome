package it.nesea.intellihome.repositories;

import it.nesea.intellihome.model.DevicePerRoom;
import it.nesea.intellihome.model.DevicePerRoomId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DevicePerRoomRepository extends JpaRepository<DevicePerRoom, DevicePerRoomId> {
}
