-- Questo script sarà eseguito al bootstrap di springboot. NOTA, il suffisso indica per quale dbms verrà utilizzato

CREATE SCHEMA IF NOT EXISTS INTELLIHOME;

CREATE TABLE IF NOT EXISTS INTELLIHOME.DEVICE(
  id serial,
  name  varchar(100) not null,
  power numeric(5,2),
  UNIQUE (name),
  PRIMARY key (id)
);

CREATE TABLE IF NOT EXISTS INTELLIHOME.ROOM(
  id serial primary key,
  name varchar(100) unique not null
);

CREATE TABLE IF NOT EXISTS INTELLIHOME.DEVICE_PER_ROOM(
  id_device integer,
  id_room integer,
  quantity integer,
  primary key (id_device, id_room),
  constraint  fk_device foreign key (id_device) references INTELLIHOME.DEVICE(id),
  constraint fk_room foreign key (id_room) references INTELLIHOME.ROOM(id)
);
