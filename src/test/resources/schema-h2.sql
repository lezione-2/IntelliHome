CREATE SEQUENCE IF NOT EXISTS intellihome.ROOM_ID_SEQ START WITH 1 INCREMENT BY 10;
CREATE SEQUENCE IF NOT EXISTS intellihome.DEVICE_ID_SEQ START WITH 1 INCREMENT BY 10;

CREATE TABLE IF NOT EXISTS INTELLIHOME.DEVICE(
  id serial,
  name  varchar(100) not null,
  power numeric(5,2),
  UNIQUE (name),
  PRIMARY key (id)
);

CREATE TABLE IF NOT EXISTS INTELLIHOME.ROOM(
  id serial primary key,
  name varchar(100) unique not null
);

CREATE TABLE IF NOT EXISTS INTELLIHOME.DEVICE_PER_ROOM(
  id_device integer,
  id_room integer,
  quantity integer,
  primary key (id_device, id_room),
  constraint  fk_device foreign key (id_device) references INTELLIHOME.DEVICE(id),
  constraint fk_room foreign key (id_room) references INTELLIHOME.ROOM(id)
);
