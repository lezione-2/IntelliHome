package it.nesea.intellihome.repositories;

import it.nesea.intellihome.model.Device;
import it.nesea.intellihome.model.DevicePerRoom;
import it.nesea.intellihome.model.Room;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class DevicePerRoomRepositoryTest {
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private DevicePerRoomRepository devicePerRoomRepository;

    /**
     * Inizializzo il DB per avere un device di controllo ed una stanza
     */
    @PostConstruct
    public void init(){
        Device device = new Device();
        device.setName("Abatjour");
        device.setPower(25d);
        deviceRepository.saveAndFlush(device);

        Room room = new Room();
        room.setName("Living Room");
        roomRepository.saveAndFlush(room);
    }

    @Test
    public void saveRelationDeviceRoom(){
        Device device = deviceRepository.findAll().get(0);
        Room room = roomRepository.findAll().get(0);
        DevicePerRoom devicePerRoom = new DevicePerRoom();
        devicePerRoom.setDevice(device);
        devicePerRoom.setQuantity(2);
        devicePerRoom.setRoom(room);
        room.getDevices().add(devicePerRoom);
        device.getRooms().add(devicePerRoom);
        devicePerRoomRepository.saveAndFlush(devicePerRoom);

        Assert.assertTrue(devicePerRoomRepository.findAll().get(0).getDevice().getId().equals(device.getId()));
        Assert.assertTrue(devicePerRoomRepository.findAll().get(0).getRoom().getId().equals(room.getId()));
    }


}
