package it.nesea.intellihome.repositories;

import it.nesea.intellihome.model.Device;
import it.nesea.intellihome.model.DevicePerRoom;
import it.nesea.intellihome.model.Room;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class RoomRepositoryTest {
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    /**
     * Inizializzo il DB per avere un device di controllo.
     */
    @PostConstruct
    public void init(){
        Device device = new Device();
        device.setName("Abatjour");
        device.setPower(25d);
        deviceRepository.saveAndFlush(device);
    }

    @Test
    public void saveRoom(){
        long size = roomRepository.count();
        Room room = new Room();
        room.setName("Living Room");
        roomRepository.saveAndFlush(room);
        Assert.assertTrue(size < roomRepository.count());
    }
}
