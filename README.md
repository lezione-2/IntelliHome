# Intelli Home
a sample project for course **Spring for Beginner**

Questo progetto è il punto di arrivo della seconda lezione. Al suo interno tratteremo varie argomentazioni:

* Startup di un progetto con Spring Boot
* Dependency Injection
* Dependency Inversion
* Component & Services
* File di Properties 
* Scope degli Spring Bean
* Unit test con Spring
* Integrazione di PostgreSQL per svil
* Integrazione di H2DB in memory
* Integrazione di Sping Data JPA
* Uso dei Repository

Per farlo useremo un esempio, molto entry level, di un impianto domotico.
Avremo modo di vedere come creare dei bean, come dire a Spring dove effettuare la scansione dei componenti 
ed analizzeremo l'autoconfig presente in spring boot.

Andremo a vedere inoltre come salvare delle configurazioni preesistenti, ad esempio, quali dispositivi 
all'interno di quale stanza.

Useremo infine il metodo Main per testare l'impianto e verificare se i dispositivi si accenderanno! 